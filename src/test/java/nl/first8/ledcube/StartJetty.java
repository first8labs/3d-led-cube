package nl.first8.ledcube;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class StartJetty {
	public static void main(String[] args) throws Exception {
        Server server = new Server(9093);

        WebAppContext context = new WebAppContext();
        context.setDescriptor("./src/main/webapp/WEB-INF/web.xml");
        
       
        context.setResourceBase("./src/main/webapp");
       // context.setResourceAlias(alias, uri);
        context.setContextPath("/");
        
        server.setHandler(context);
       
        server.start();
        server.join();
        
        
	}
}
