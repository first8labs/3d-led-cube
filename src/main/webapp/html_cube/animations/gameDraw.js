gameDraw = {
		refresh : 25,	
	name : 'Draw the Cube',
	coloredList : [ 111, 123, 134, 144 ],
	counter :0, 
	pause : false, 
	getAnimation : function(cube) {

		if (this.pause||!cube) {
			if (!cube) {
				console.log('draw.no cube');
			}
			return frames;
		}
		for (var index = 0; index < this.coloredList.length; index++) {
			var idNr = this.coloredList[index];
			voxel ={}; 
			voxel.x = (Math.floor(idNr / 100)) - 1;
			voxel.y = (Math.floor((idNr % 100) / 10)) - 1;
			voxel.z = (idNr % 10) - 1;

			cube.setVoxel(voxel.x, voxel.y, voxel.z);
			console.log('draw.voxel ' +index+ ' voxel '+ JSON.stringify(voxel) + ' ' +this.coloredList[index]); 
		}
		console.trace('draw.voxel ' +this.coloredList.length + ' voxel '+ JSON.stringify(this.coloredList) + ' -- ' + cube.count()); 
		
	
		
		this.counter++; 
		if (this.counter>2) {
			this.pause = true; 
		} else {
			cube.nextFrame();
		}
		
		return frames;
	},
	reset : function() {
		this.coloredList = [];
		this.pause = false; 
		this.counter=0;
		$('.LED div').removeClass('on');
	},
	toggleColorItem : function(idNr) {
		var pos = this.coloredList.indexOf(idNr);

		if (pos == -1) {
			this.coloredList.push(idNr);
			this.coloredList.sort();
		} else {
			this.coloredList.splice(pos, 1);
		}
		this.pause = false; 
	}
}
