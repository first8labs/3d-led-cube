temp = {
	name    : 'Corner Cube',
	refresh : 50, // ms -- maximum refresh = 58fps or 17ms

	/**
	 * Animation should return an array of dimensions [N][z][x][y] where:
	 * - Note: z comes first
	 * - N is the number of frames in the animation
	 * - x, y, z are the width, depth and height of the cube respectively
	 * - 0, 0, 0 is the bottom-front-left of the cube
	 * - 7, 7, 7 is the back-top-right of the cube in an 8x8x8 cube
	 *
	 * All values in the array should be 0 or 1 (off/on)
	 **/
	getAnimation : function(cube){
		//console.log('cube '+cube)
		//console.log(cube)
		
		for(i=0; i<8; i++){
			if (i==0||i==7) {
				cube.setPlaneY(i);
				cube.setPlaneZ(i);
			}
		}
		cube.nextFrame(true); 
		//cube.nextFrame();
		return frames;
	}
}