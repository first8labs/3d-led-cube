gameRows = {
	name : '4 in a row',
	refresh : 25, // ms -- maximum refresh = 58fps or 17ms
	positionHead : [ 0, 0, 0 ],
	stopped: false,
	pause: false,
	historyList : [], 
	pointListMap : {}, 
	lineListMap :{},
	heightMap :{},
	stop:function() {
		this.stopped = true; 
		$('.on div').css('background-color','black'); 
	},
	reset : function() {
		this.historyList=[]; 
		this.stopped = false; 
		this.pause = false; 
		$('.LED div').css('background-color','');
		$('.LED div').removeClass('on');
	},
	getAnimation : function(cube) {
		var isWhite = true; 
		this.heightMap = {}; 
		this.pointListMap[0]=[];
		this.pointListMap[1]=[];
		
		for (var index=0;index<this.historyList.length;index++) {
			var voxelX = this.historyList[index];
			if (!this.heightMap[voxelX]) {
				this.heightMap[voxelX]=0;
			}
			this.heightMap[voxelX]++;
			if (voxelX) {
				var pos = 6 + (index+1) % 2; 
				
				if (cube) {
					cube.setVoxel( pos, voxelX-1, this.heightMap[voxelX]-1);
				}
				
				var point = parseFloat((voxelX-1)+'.'+ (this.heightMap[voxelX]-1));
				this.pointListMap[(index) % 2].push(point);
			}
		}
		if (this.stopped) {
			this.stop()
		}
		if (cube) {
			cube.nextFrame();
		}
		
		
		return frames;
	},
	currentPlayer : function() {
		return this.historyList.length % 2; 
	}	
	,
	insertPiece : function(rowInt) {
		if (!this.stopped) {
			var current = this.currentPlayer();
			
			this.historyList.push(rowInt); 
			this.getAnimation(null);
			var hasFinished = this.computePointList(current); 
			
			if (hasFinished) {
				this.stop(); 
			}
		}
		
	},
	computePointList : function(userInt) {
		this.lineListMap ={}; 
		
		list = this.pointListMap[userInt];
		var hasLine4 = false; 
		for (var index=0;index<list.length;index++) {
			var current = list[index];
			
			if (list.indexOf(current+0.1)!=-1) {
				var has3 = list.indexOf(current+0.2)!=-1;
				hasLine4 = has3&&list.indexOf(current+0.3)!=-1;
				if (hasLine4) {
					break; 
				}
			}
			if (list.indexOf(current+1)!=-1) {
				var has3 = list.indexOf(current+2)!=-1;
				hasLine4 = has3&&list.indexOf(current+3)!=-1;
				if (hasLine4) {
					break; 
				}
			}
			if (list.indexOf(current+1.1)!=-1) {
				var has3 = list.indexOf(current+2.2)!=-1;
				hasLine4 = has3&&list.indexOf(current+3.3)!=-1;
				if (hasLine4) {
					break; 
				}
			}
			if (list.indexOf(current-1.1)!=-1) {
				var has3 = list.indexOf(current-2.2)!=-1;
				hasLine4 = has3&&list.indexOf(current-3.3)!=-1;
				if (hasLine4) {
					break; 
				}
			}
		}
		console.log('hasLine4 ' +userInt + ' ' +hasLine4)
		
		return hasLine4;
	}
}
