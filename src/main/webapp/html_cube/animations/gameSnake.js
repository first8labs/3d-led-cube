gameSnake = {
	name : 'Snake Game',
	refresh : 150, // ms -- maximum refresh = 58fps or 17ms
	positionHead : [ 0, 0, 0 ],
	direction : 3,
	score:0,
	level:1, 
	snakeList : [ ],
	candyList : [ ], 
	snakeLength : 1,
	manual: false,
	pause: false,
	isStep:false,
	counter: 0, 
	reset : function() {
		this.snakeList=[];
		this.candyList=[];
		this.snakeLength=1;
		this.score=0;
		this.pause=false; 
		this.counter=0; 
		this.insertRandomCandy(); 
	},
	isNodeOpen: function( direct) {
		///var newNode = [this.positionHead[0],this.positionHead[1],this.positionHead[2]];
		return !this.isNodeSnake(this.positionHead, this.direction); 
	},
	isNodeSnake : function(node, direct) {
		var newNode = [node[0],node[1],node[2]];

		var pos = (direct - 1);
		if (direct < 0) {
			pos = (-(direct + 1));
		}	
		if (direct> 0) {
			newNode[pos]++;
			if (newNode[pos] >= CUBE.size[0]) {
				newNode[pos] = 0;
			}
		} else {
			
			newNode[pos]--;
			if (newNode[pos] < 0) {
				newNode[pos] = CUBE.size[0] - 1;
			}
			
		}
		var isResult = JSON.stringify(this.snakeList).indexOf(JSON.stringify(newNode))!=-1;
		
		//console.log('isResult ' +isResult+ '  direct ' +direct + ' pos '+pos+' ' +JSON.stringify(newNode));
		
		return isResult;
		
	},
	getDirections : function() {
		var result = []; 
		result.push(this.isNodeSnake(this.positionHead,-3)); 
		result.push(this.isNodeSnake(this.positionHead,-2)); 
		result.push(this.isNodeSnake(this.positionHead,-1)); 
		result.push(this.isNodeSnake(this.positionHead,1)); 
		result.push(this.isNodeSnake(this.positionHead,2)); 
		result.push(this.isNodeSnake(this.positionHead,3)); 
		
		return result; 
	},
	getNextOpenDirection : function() {
		var boolArray = this.getDirections(); 
		var fIndex=-1; 
		for (var index=0;index<boolArray.length;index++){
			if (!boolArray[index]) {
				fIndex = index; 
				break; 
			} 
		}
		var result = 0; 
		if (fIndex!=-1) {
			result = fIndex-3; 
			if (result>=0) {
				result++;
			}
		}
		console.log('fresh direction: ' +result+ " " +JSON.stringify(this.getDirections()) + " " + result);
		return result; 
	},
	getNextNode : function(amount) {
		if (amount) {
			this.counter++; 
			if (this.counter>500) {
				this.stop(); 
			}
		}
		var delta = this.getDirectionCandy(); 
		
		if (delta[0]==delta[1]&&delta[2]==delta[1]&&delta[0]==0) {
			this.score++;
			this.snakeLength++;
			this.insertRandomCandy();
			this.candyList.shift();
			console.log('candy found, score ' +this.score );
			return this.getNextNode();
		}
		var pos = (this.direction - 1);
		if (this.direction < 0) {
			pos = (-(this.direction + 1));
		}	
		
		if (!this.manual) {
			
			
			if (delta && delta[pos]==0&&!amount) {
				this.direction = ((this.direction+1)%3) +Math.round(Math.random());
				if (!this.isNodeOpen(this.direction)) {
					console.log('closed direction3: ' +this.direction + " " +JSON.stringify(this.getDirections()) + " " + pos);

					this.direction = ((this.direction+1)%3) +1;
					
					var pos = (this.direction - 1);
					if (this.direction < 0) {
						pos = (-(this.direction + 1));
					}	
					console.log('new direction3: ' +this.direction + " " +this.isNodeOpen(this.direction) + " " + pos);
					/*
					if (delta[pos]==0||!this.isNodeOpen(this.direction)) {
						console.log('closed direction5: ' +this.direction + " " +JSON.stringify(this.getDirections()) + " " + pos);

						this.direction = ((this.direction+1)%3) +1;
						
						if (!this.isNodeOpen(this.direction)) {
							this.stop();
							return null; 
						}
					}
					*/ 
				}
				return this.getNextNode(1);
			} else {
				if (!this.isNodeOpen(this.direction)) {
					console.log('closed direction: ' +this.direction + " " +JSON.stringify(this.getDirections()) );
					
					this.direction = ((pos +2)%3) +1;
					console.log('new direction: ' +this.direction  + ' ' +this.isNodeOpen(this.direction));

					if (!this.isNodeOpen(this.direction)) {
						console.log('closed direction2: ' +this.direction + " " +JSON.stringify(this.getDirections()) );

						this.direction = this.getNextOpenDirection();
						console.log('new direction2: ' +this.direction );
						
						if (this.direction==0) {
							
							this.stop();
							return null; 
						}
					}
					
					return this.getNextNode();
				}
			}
		}
		if (this.direction > 0) {
			this.positionHead[pos]++;
			if (this.positionHead[pos] >= CUBE.size[0]) {
				this.positionHead[pos] = 0;
			}
		} else {
			
			this.positionHead[pos]--;
			if (this.positionHead[pos] < 0) {
				this.positionHead[pos] = CUBE.size[0] - 1;
			}
			
		}
		return [this.positionHead[0],this.positionHead[1],this.positionHead[2]];
	},
	getAnimation : function(cube) {

		if (this.pause&&!this.isStep) {
			return frames; 
		}
	    if (this.candyList.length==0) {
	    	this.insertRandomCandy();
	    }
		
		var newNode = this.getNextNode();
		if (!newNode) {
			return frames; 
		}
		
		if (this.snakeList.length>=this.snakeLength) {
			this.snakeList.shift();
			if (this.snakeList.length>=this.snakeLength) {
				this.snakeList.shift();
			}
		}
		
		var isNew = JSON.stringify(this.snakeList).indexOf(JSON.stringify(newNode))==-1;
		if (isNew) {
			this.snakeList.push(newNode)
		} else {
			this.stop(); 
		}
		
		for (var index=0;index<this.snakeList.length;index++) {
			var voxel = this.snakeList[index];
			if (voxel) {
				cube.setVoxel(voxel[0], voxel[1], voxel[2]);
			}
		}
		for (var index=0;index<this.candyList.length;index++) {
			var voxel = this.candyList[index];
			if (voxel) {
				cube.setVoxel( voxel[0], voxel[1], voxel[2]);
			}
		}
		
		cube.nextFrame(true);
		
		if (this.isStep) {
			this.isStep = false; 
			animationConfig.updateMenu();
		}

		return frames;
	},
	stop:function() {
		this.pause = true; 
		console.trace('crashed, turns counted ' +this.counter); 
		$('.on div').css('background-color','black'); 
		this.counter=99999; 
	}
	, 
	getDirectionCandy : function() {
		var ph = this.positionHead; 
		if (this.candyList.length==0) {
			return [1,1,1]; 
		}
		var pc = this.candyList[0]; 
		var delta = [ph[0]-pc[0],ph[1]-pc[1],ph[2]-pc[2]];
		return delta; 
	},
	insertRandomCandy : function() {
		var newNode = [Math.round(Math.random()*7),Math.round(Math.random()*7),Math.round(Math.random()*7)] ;
		
		var isNew = JSON.stringify(this.snakeList).indexOf(JSON.stringify(newNode))==-1;
		isNew = isNew && JSON.stringify(this.candyList).indexOf(JSON.stringify(newNode))==-1;
		if (isNew) {
			this.candyList.push(newNode)
			return newNode; 
		} else {
			return this.insertRandomCandy();  
		}
	}
	
}
