/**
 * Control interaction with the cube itself
 */
window.onload=function() {
	
	onloadCube(); 
}

function onloadCube() {
	rotate= {
			 X : 0,
			 Y : 0
	}; 
	animationConfig = new AnimationConfig();
	animationConfig.initScripts('gameSnake','gameDraw','gameRows','rain','game_of_life','spinning_line','corner_cube','center_sine_wave','planes').setDimensions(8, 8, 8).init();
	animationConfig.games = []; 
	animationConfig.games.push(gameSnake); 
	animationConfig.games.push(gameDraw); 
	animationConfig.games.push(gameRows); 
	
	moveable = document.getElementById('moveable'), nav = document
			.getElementById('navigation'), dragging = false, targetY = 330, dragStart = {}, lastDrag = 0;

	moveable.style.backgroundColor = 'yellow';
	document.body.addEventListener('mouseup', mouseUp);
	document.body.addEventListener('keydown', keyDown);
	moveable.addEventListener('mousedown', mouseDown);
	moveable.addEventListener('mouseup', mouseUp);
	moveable.addEventListener('mousemove', mouseMove);
	moveable.addEventListener('mouseleave', mouseUp);
	 
}
function isFrameContent() {
	return window.parent!=null&&window!=window.parent; 
}
function keyDown(ev) {
	console.log('keyDown ');
	
	if (ev.which>=49 &&ev.which<=56) {
		
		gameRows.insertPiece((ev.which-48))
	}
	if (ev.which==40) {
		gameSnake.direction=-3; 
	}
	if (ev.which==38) {
		gameSnake.direction=3; 
	}
	if (ev.which==37) {
		gameSnake.direction=-2; 
	}	
	if (ev.which==39) {
		gameSnake.direction=2; 
	}
	if (ev.which==87) {
		gameSnake.direction=1; 
	}
	if (ev.which==83) {
		gameSnake.direction=-1; 
	}
	
	//console.log(ev.which + ' ' + animationConfig.animationList[0].snakeLength);
}
function mouseDown(ev) {
	console.log('mouseDown ');
	dragging = true;
	dragStart = {
		x : ev.pageX,
		y : ev.pageY
	};
}

function mouseUp(ev) {
	console.log('mouseUp ');
	
	if (dragging) {
		dragging = false;
		setRotationUpdate(ev); 
	}
}

function mouseMove(ev) {
	if (!isLeftMouseButtonPressed(ev)) {
		dragging = false; 
	}
	
	if (!dragging) {
		return;
	}
	console.log('mouseMove ' + dragging);
	lastDrag = new Date();
	setRotationUpdate(ev); 
}
function setRotationDefault() {
	
	setRotation(0, 0, true);
	gameSnake.pause=true;
	gameDraw.pause=true;
	gameRows.pause=true;
	if (animationConfig.active==false) {
		animationConfig.startAnimation();
	}
	if (animationConfig.isCurrentGame(gameSnake)) {
		gameSnake.reset(); 
	}
	if (animationConfig.isCurrentGame(gameDraw)) {
		
		gameDraw.reset(); 
	}
	if (animationConfig.isCurrentGame(gameRows)) {
		gameRows.reset(); 
	}
}
function setRotationUpdate(ev) {
	setRotation(rotate.X + (ev.pageX - dragStart.x)/2, rotate.Y + (ev.pageY - dragStart.y)/2,
			true)
}
function setRotation(rotx, roty, store) {
	if (isNaN(rotx)) {
		setRotationDefault(); 
		return; 
	}
	console.log('setRotation ' + rotx + ' ' + roty + ' ' + store);
	canvas.style.WebkitTransform = 'rotateX(' + roty + 'deg) rotateY(' + rotx + 'deg)';
	canvas.style.border ='2px black solid';
	
	var points = animationConfig.points;
	for (var i = 0, l = points.length; i < l; i++) {
		for (var j = 0, m = points[i].length; j < m; j++) {
			for (var k = 0, n = points[i][j].length; k < n; k++) {
				points[i][j][k].children[0].style.WebkitTransform ='rotateX(' + -roty + 'deg) rotateY(' + -rotx + 'deg)';
			}
		}
	}

	if (store) {
		rotate.X = rotx;
		rotate.Y = roty;
	}
}


function isLeftMouseButtonPressed(evt) {
    evt = evt || window.event;
    var button = evt.which || evt.button;
    return button == 1;
}