function AnimationConfig() {
	this.animationList = [];
}

AnimationConfig.prototype.initScripts = function() {
	this.animationList = [];
	for (var i = 0, l = arguments.length; i < l; i++) {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', './animations/' + arguments[i] + '.js', false);
		xhr.send();
		this.animationList.push((new Function('return ' + xhr.responseText))());
	}

	return this;
}

AnimationConfig.prototype.setDimensions = function(x, y, z) {
	this.x = x;
	this.y = y;
	this.z = z;

	return this;
};

AnimationConfig.prototype.init = function() {
	var spacing = 50, cubeView = document.getElementById('canvas'),

	x, y, z, layer, row, point, randLI;

	this.points = [];

	// create all of the LEDs
	for (z = 0; z < this.z; z++) {
		layer = [];
		for (x = 0; x < this.x; x++) {
			row = [];
			for (y = 0; y < this.y; y++) {
				point = document.createElement('DIV');
				point.className = 'LED';
				point.style.cssText = '-webkit-transform: translate3d('
						+ (spacing * y) + 'px, ' + (spacing * (this.z - z - 1))
						+ 'px, ' + (spacing * x) + 'px)';

				point.appendChild(document.createElement('DIV'));

				cubeView.appendChild(point);

				
				
				row.push(point);
			}
			layer.push(row);
		}
		this.points.push(layer);
	}
	var self = this; 
	$('.LED').click(function(event) {
		var idNr = parseInt(this.title); 
		if (self.isCurrentGame(gameDraw)) {
			
			gameDraw.toggleColorItem(idNr); 
			console.log('coloredList ' +JSON.stringify(gameDraw.coloredList) + ' '+idNr );
			
			gameDraw.pause=false; 
			gameDraw.counter=0; 
		}
		if (self.isCurrentGame(gameRows)) {
			console.log('historyList ' +JSON.stringify(gameRows.historyList) + ' '+idNr);
			alert('use keys [1..8] for inserting a piece');
		}
		event.stopPropagation();
	})
	this.selectAnimation();
	this.createMenuForModules();
	this.createMenu(this.getCurrentGameName());
};
AnimationConfig.prototype.createMenu = function(type) {
	$('#navigation').css('display', 'block');
	
	menu = $('#navmenucontent');
	menu.html('');
	if (!type) {
		type=this.getCurrentGameName();
	}
	//this.createMenuForModules();
	
	menu
	.append('<input type="button" value="restart" onclick="setRotationDefault()"/>');
	menu
	.append('<input type="button" value="stop" onclick="animationConfig.toggleAnimation()"/>');
	
	menu
	.append('<input type="button" value="save" onclick="animationConfig.externalSave()"/>');
	
	menu
	.append('<input type="checkbox" value="" id="autoSave" onclick="enableAutoSave(this.checked)" />');
	
	
	if (type.indexOf('draw')!=-1) {
		menu
		.append('<input type="button" value="read" onclick="animationConfig.externalRead()"/>');

	}
	
	$('#keypad').hide(); 
	if (type.indexOf('snake')!=-1) {
		menu
		.append('<input type="button" value="auto" onclick="gameSnake.manual=!gameSnake.manual;"/>');
		menu
		.append('<input type="button" value="step" onclick="gameSnake.isStep=true;"/>');
	
		$('#keypad').show(); 
	}
	if (type.indexOf('game')>0) {
		menu
		.append('<input type="button" value="pause" onclick="animationConfig.togglePause();"/>');

	}
	if (type.indexOf('snake')!=-1) {
		menu.append('<br />');
		menu.append('<div>Score: <span id="score">0</span></div>');
		menu.append('<div>Level: <span id="level">1</span></div>');
		menu.append('<div>Position: <span id="xyz"></span></div>');
		menu.append('<div>Length: <span id="length"></span></div>');
		menu.append('<div>Rotation: <span id="rotation"></span></div>');
		menu.append('<div>Candy: <span id="candy"></span></div>');
		menu.append('<div>Delta: <span id="delta"></span></div>');
		menu.append('<div>Direction: <span id="direction"></span></div>');
		menu.append('<div>Closed: <span id="open"></span></div>');
		$('#speed').val(gameSnake.refresh);
		gameSnake.pause=isFrameContent(); 
	}
	

	
	$('input').each(function() {
		$(this).attr('title',$(this).attr('onclick'))
	})
	$('select').each(function() {
		$(this).attr('title',$(this).attr('onchange'))
	})
};
AnimationConfig.prototype.updateMenu = function() {
	$('#score').html(gameSnake.score);
	$('#level').html(gameSnake.level);
	$('#xyz').html(JSON.stringify(gameSnake.positionHead));
	$('#length').html(gameSnake.snakeList.length);
	$('#length').attr('title', JSON.stringify(this.snakeList));

	$('#rotation').html(JSON.stringify(rotate).replace(/"/g, ''));
	if (gameSnake.candyList && gameSnake.candyList.length>0) {
		$('#candy').html(JSON.stringify(gameSnake.candyList[0]).replace(/"/g, ''));
	}
	
	$('#delta').html(
			JSON.stringify(gameSnake.getDirectionCandy()).replace(/"/g, ''));
	$('#direction').html(gameSnake.direction);
	$('#open').html(JSON.stringify(gameSnake.getDirections()).replace(/false/g, 'f').replace(/true/g, 't'));

	$('#keypad .fa').css('color', 'black');
	var directionStr = 'plus' + gameSnake.direction;
	var redStr = 'min' + gameSnake.direction;
	if (gameSnake.direction < 0) {
		directionStr = 'min' + (-gameSnake.direction);
		redStr = 'plus' + (-gameSnake.direction);
	}
	$('#' + directionStr).css('color', 'green');
	$('#' + redStr).css('color', 'red');
},

AnimationConfig.prototype.createMenuForModules= function() {
	// create the navigation
	var self = this; 
	$('#navmenu').append('<div id="nr" style="display:inline"></div><br/><select id="modules" style="width:150px" onchange="animationConfig.createMenu();setRotationDefault();animationConfig.selectAnimation(this.selectedIndex);"></select>'); 
	var sModules = $('#modules')[0];
	
	for (var i = 0, l = this.animationList.length; i < l; i++) {
		sModules.options.add(new Option(this.animationList[i].name,this.animationList[i].name)); ;
	}
	 
}
AnimationConfig.prototype.isCurrentGame=function(gameObj) {
	return gameObj.name&&gameObj.name.toLowerCase()==this.getCurrentGameName();
}

AnimationConfig.prototype.getCurrentGameName=function() {
	var result = $('#modules').val();
	if (result) {
		result = result.toLowerCase()
	} else {
		result = ''; 
	}
	return  result; 
}
AnimationConfig.prototype.getCurrentGame=function() {
	var name = this.getCurrentGameName(); 
	for (var index =0;index<animationConfig.animationList.length;index++ ) {
		var obj = animationConfig.animationList[index];
		if (obj.name.toLowerCase()==name) {
			return obj; 
		}
	}
}
AnimationConfig.prototype.getCurrentGameIndex=function() {
	var name = this.getCurrentGameName(); 
	for (var index =0;index<animationConfig.animationList.length;index++ ) {
		var obj = animationConfig.animationList[index];
		if (obj.name.toLowerCase()==name) {
			return index; 
		}
	}
}
AnimationConfig.prototype.selectAnimation = function(anim) {
	//console.trace('anim '+anim)
	if (!anim) {
		anim = this.animationList[0];
	} else
	if (!isNaN(anim)){
		animationConfig.cube.frame = parseInt(anim);
		anim = this.animationList[animationConfig.cube.frame];
	}
	
	var isSame = !this.isCurrentGame(gameSnake) && this.currentAnimation  == anim; 
	
	this.currentAnimation = anim;
	this.stopAnimation();
	this.startAnimation(isSame);
};
AnimationConfig.prototype.resetCube = function() {
	this['cube'] = new Cube(this.x, this.y, this.z);	
}
AnimationConfig.prototype.startAnimation = function(isSame) {
	var self = this;
	this.active=true; 
	
	
	this.resetCube();
		
	//this.cube.drawLine(0,0,0,7,7,0); 
	self.currentAnimation.getAnimation(this.cube);

	this.cube.getFrame(0); // init at 0

	this.interval = setInterval(function() {
		self.nextFramePresent();
		if (!gameSnake.pause ) {
			self.updateMenu();
		}
	
	}, this.currentAnimation.refresh);
}

AnimationConfig.prototype.stopAnimation = function() {
	this.active=false; 
	//console.log('stopAnimation ' +this.interval)
	this.interval && clearInterval(this.interval);
};
AnimationConfig.prototype.toggleAnimation = function() {
	if (this.active) {
		this.stopAnimation();
	} else {
		this.startAnimation();
	}
};
AnimationConfig.prototype.togglePause = function() {
	this.getCurrentGame().pause = !this.getCurrentGame().pause; 
	console.log('toggle ' + this.getCurrentGame().pause)
};



AnimationConfig.prototype.externalRead= function() {
	var url =   this.externalUri() + '/rest/cube/bulkjson';
	console.log('url ' +url);  
	var self = this; 
	$.get( url, function( data ) {
		console.log('externalRead ' + (typeof data));
		console.log(data);
		window.data = window.eval('('+data+')').cube;
		self.cube.loadText(data); 
		self.nextFramePresent(true); 
	});
	
}
function enableAutoSave(isStart) {
	
	if ($('#autoSave').prop('checked')||isStart) {
		window.setTimeout('enableAutoSave(false)',100);
		animationConfig.externalSave(); 
	}
}
AnimationConfig.prototype.externalUri= function() {
	if (location.pathname.indexOf('ledcube')==-1) {
		return window.location.origin
	} else {
		return window.location.origin + 'ledcube/'
	}
}
AnimationConfig.prototype.externalSave= function() {
	console.clear();
	console.log('frame ' + this.cube.frame + ' ' +this.cube.frames.length); 
	console.log(); 
	var self = this; 
	var url =  this.externalUri() + '/rest/cube/bulk';
	$.ajax({
	  type: "POST",
	  url: url,
	  data: self.cube.toText(),
	  success: function() {
		  console.log('externalSaved'); 
	  }
	});
}
Cube.prototype.toText=function() {
	var txt = ''; 
	for (var x = 0; x < this.size.x; x++) {
		for (var y = 0; y < this.size.y; y++) {
			for (var z = 0; z < this.size.z; z++) {
				txt += this.getVoxel(x, y, z) ? '1' : '0';
			}
			txt += '\n';
		}
		txt += '\n';
	}
	return txt;
}
Cube.prototype.count=function() {
	var txt = 0; 
	for (var x = 0; x < this.size.x; x++) {
		for (var y = 0; y < this.size.y; y++) {
			for (var z = 0; z < this.size.z; z++) {
				txt += this.getVoxel(x, y, z) ? 1 : 0;
			}
		}
	}
	return txt;
}
Cube.prototype.loadText=function(txt) {
	gameDraw.coloredList=[]; 
	counter = 0; 
	window.pages = window.data.replace(/  /g,'|').split('|');
	
	for (var x = 0; x < pages.length; x++) {
		var page = pages[x]; 
		var lines = page.split(' ');
		console.log('lines ' +lines.length);
		for (var y = 0; y < lines.length; y++) {
			var line = lines[y]; 
			
			for (var z = 0; z < line.length; z++) {
				if (line.charAt(z)!='1') {
					this.clearVoxel(x,y,z);
					
				} else {
					this.setVoxel(x,y,z)
					counter++;
				}
			}
			console.log('line ' +lines.length + ' ' + counter);
		}
	}
	console.log('pages ' +window.pages.length  + ' count ' +this.count());
	return txt;
}
AnimationConfig.prototype.nextFramePresent = function(isDoWrite) {
	
	
	if ((this.cube.frame == this.cube.frames.length - 1)&&!isDoWrite) {
		//console.log('nextFramePresent selectAnimation');
		
		this.selectAnimation($('#modules').prop('selectedIndex'));
		return;
	}
	$('#appTitle').attr('title',this.cube.frame + "/"+this.cube.frames.length + '/' +this.cube.count());
	var count = 0; 
	for (var x = 0; x < this.x; x++) {
		for (var y = 0; y < this.y; y++) {
			for (var z = 0; z < this.z; z++) {
				this.points[z][x][y].className = 'LED'
						+ (this.cube.getVoxel(x, y, z) ? ' on' : '');
				
				if (this.cube.getVoxel(x, y, z)) {
					count++; 
				}
				var idNr = '' +((x+1)*100+(y+1)*10+(z+1));
				this.points[z][x][y].title=idNr; 
			}
		}
	}
	if (isDoWrite) {
		console.log('test ' +count)
	}
	this.cube.nextFrame();
}
