package nl.first8.ledcube;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;

/**
 * TODO rewrite to websockets
 * @author arjanl
 *
 */
public class WebOutputCube implements CubeOutput {

    private static final String LEDCUBE_URL = "http://ledcube.first8.nl/rest/cube";

    @Override
    public String getName() {
        return "WebOutputCube";
    }

    @Override
    public void init() throws CubeException {

    }

    @Override
    public void flush() {
    }

    @Override
    public void setPixel(int x, int y, int z, boolean value) {
        
        try {
        String u = LEDCUBE_URL + "/" +x + "/" + y + "/" + z + "/" + (value?"1":"0");
        System.out.println("\nSending 'POST' request to URL : " + u);
        
        URL url = new URL(u);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "LedCubeDesktop");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        
        // Send post request
//        con.setDoOutput(true);
//        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//        wr.writeBytes(urlParameters);
//        wr.flush();
//        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        
        //print result
        System.out.println(response);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getPixel(int x, int y, int z) {
        return false;
    }

    @Override
    public void setCube(boolean[][][] cube) {
        try {
            String output = StringCubeUtil.arrayToString(cube);
        String u = LEDCUBE_URL + "/bulk";
        System.out.println("\nSending 'POST' request to URL : " + u);
        
        URL url = new URL(u);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "LedCubeDesktop");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        
        // Send post request
        con.setDoOutput(true);
        
        IOUtils.write(output, con.getOutputStream(), "UTF-8");
        IOUtils.closeQuietly(con.getOutputStream());

        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        
        //print result
        System.out.println(response);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

}
