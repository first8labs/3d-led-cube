package nl.first8.ledcube.gui;

public enum Configuration {
    
    SNAKE, WEB, EFFECTS;
    

    public boolean startSnake() {
        return this==SNAKE;
    }

    public boolean isSnakeOutput() {
        return this==SNAKE;
    }

    public boolean isSnakeInput() {
        return this==SNAKE;
    }

    public boolean isSysOutOutput() {
        return this==WEB;
    }

    public boolean isWebOutput() {
        return this==WEB;
    }

    public boolean isWebInput() {
        return this==WEB;
    }

    public boolean isUsbOutput() {
        return true;
    }

    public boolean isScreenInput() {
        return true;
    }

    public boolean isScreenOutput() {
        return true;
    }

    public boolean isEffectsInput() {
        return this==Configuration.EFFECTS;
    }

}
