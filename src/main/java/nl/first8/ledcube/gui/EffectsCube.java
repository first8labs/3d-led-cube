package nl.first8.ledcube.gui;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import nl.first8.ledcube.Effects;

import nl.first8.ledcube.CubeException;
import nl.first8.ledcube.InputOutputCube;
import nl.first8.ledcube.UsbCube;

public class EffectsCube extends InputOutputCube {

    private UsbCube usbCube;

    public EffectsCube(UsbCube usbCube) {
        this.usbCube = usbCube;
    }
    
    @Override
    public String getName() {
        return "effects";
    }

    @Override
    public void init() throws CubeException {
        Thread t = new Thread(() -> {
            for (;;) {
                Effects.fadeIn(usbCube);
                Effects.fadeOut(usbCube);
//                interactive();
            }
        });
        t.setDaemon(true);
        t.start();
        
    }

    private void interactive() {
        System.out.println("Which effect?");
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        try {
            String effect;
            effect = r.readLine();
            switch(effect) {
            case "0": Effects.grey(usbCube, 10000, 0); break;
            case "1": Effects.grey(usbCube, 10000, 1); break;
            case "2": Effects.grey(usbCube, 10000, 2); break;
            case "3": Effects.grey(usbCube, 10000, 3); break;
            case "4": Effects.grey(usbCube, 10000, 4); break;
            case "fadein": Effects.fadeIn(usbCube); break;
            case "fadeout": Effects.fadeOut(usbCube); break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    
}
