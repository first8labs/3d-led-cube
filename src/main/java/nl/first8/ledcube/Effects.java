package nl.first8.ledcube;

/**
 * Use these effects only on USB cube since the rest is probably too slow to compete. 
 */
public class Effects implements Cube {
    
    private static final boolean[][][] OFF = new boolean[WIDTH][HEIGHT][DEPTH];
    private static final boolean[][][] ON = new boolean[WIDTH][HEIGHT][DEPTH];
    static {
        for (int x=0;x<WIDTH;x++ ) {
            for (int y=0;y<HEIGHT;y++ ) {
                for (int z=0;z<WIDTH;z++ ) {
                    OFF[x][y][z] = false;
                    ON[x][y][z] = true;
                 }
            }
        }
    }

    /**
     * 
     * @param c the cube to use the effect on
     * @param duration the duration in ms
     * @param level 0 (black) to 4 (white)
     */
    public static void grey(UsbCube c, long duration, int level) {
        if (c==null) return;
        
        long start = System.currentTimeMillis();
        
        while ( System.currentTimeMillis()-start<duration) {
            for (int i=0; i<(4-level); i++) {
                c.setCube(OFF);
                c.flush();
            }
            if (level>0) {
                c.setCube(ON);
                c.flush();
            }
        }
        
    }

    public static void fadeIn(UsbCube c) {
        if (c==null) return;
        Effects.grey(c, 300, 0); 
        Effects.grey(c, 300, 1); 
        Effects.grey(c, 300, 2); 
        Effects.grey(c, 300, 3); 
        Effects.grey(c, 300, 4); 
    }
    public static void fadeOut(UsbCube c) {
        if (c==null) return;
        Effects.grey(c, 300, 4); 
        Effects.grey(c, 300, 3); 
        Effects.grey(c, 300, 2); 
        Effects.grey(c, 300, 1); 
        Effects.grey(c, 300, 0); 
    }

    public static void black(UsbCube c) {
        if (c==null) return;
        c.setCube(OFF);
        c.flush();
    }

    public static void white(UsbCube c) {
        if (c==null) return;
        c.setCube(ON);
        c.flush();
    }

}
