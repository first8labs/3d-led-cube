# Ledcube-server

This is a Java application meant to connect a Led Cube to the "Cloud".

The project actually contains two runnables, a standalone JavaFX application that can interact with the cube via USB, render the Cube on screen and play games.
It can also be deployed as a WAR archive on a JEE container, providing a RESTful interface for the cube.

---

*Usage:*

1. WildFly: this project can be deployed to WildFly as a WAR file named ledcube.war. 

2. Jetty: start application via typing: 'mvn clean compile jetty:run' (and navigate towards http://localhost:9093/html_cube/) 

3. JavaFX: start the main class LedCubeApplication
